package cn.ce.common.cron4j;

import com.jfinal.kit.PathKit;

import cn.ce.common.kit.DelFolderOrAllFile;

public class DelImg implements Runnable {

	public String getAvatarTempDir() {
		return "/upload/avatar/temp/";
	}
	public String getGenDir(){
		return "/avatar/gendir/";
	}
	
	@Override
	public void run() {
		DelFolderOrAllFile.delAllFile(PathKit.getWebRootPath()+getAvatarTempDir());
		DelFolderOrAllFile.delAllFile(PathKit.getWebRootPath()+getGenDir());
	}

}
