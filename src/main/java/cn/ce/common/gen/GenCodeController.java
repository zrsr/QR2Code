package cn.ce.common.gen;

import java.io.File;

import javax.servlet.http.HttpSession;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.upload.UploadFile;

import cn.ce.common.index.QRCodeUtil;
import cn.ce.common.kit.ImageKit;

public class GenCodeController extends Controller {

	
	// 用户上传图像最多只允许 1M大小
	public int getAvatarMaxSize() {
		return 1024 * 1024;
	}

	/**
	 * 上传文件，以及上传后立即缩放后的文件暂存目录
	 */
	public String getAvatarTempDir() {
		return "/avatar/temp/";
	}
	public String getGenDir(){
		return "/avatar/gendir/";
	}
	
	// 经测试对同一张图片裁切后的图片 jpg为3.28KB，而 png 为 33.7KB，大了近 10 倍
	public static final String extName = ".jpg";
		
	
	/**
	 * 
	 * @Title: genCode   
	 * @Description: TODO : 带logo二维码生成
	 * @param: @throws Exception      
	 * @author: 冉志林    
	 * @date:   Oct 19, 2018 6:20:06 PM  
	 * @return: void      
	 * @throws
	 */
	public void genCode() throws Exception {
		String url = getPara("title");
		if(StrKit.isBlank(url)){
			renderJson(Ret.fail("msg", "请输入地址"));
			return;
		}
		String img=getSessionAttr("avatarUrl").toString();
		System.out.println(img);
		if(StrKit.isBlank(img)){
			renderJson(Ret.fail("msg", "请先上传图片"));
			return;
		}
		QRCodeUtil.encode(url, PathKit.getWebRootPath()+img, PathKit.getWebRootPath()+getGenDir(), true,getSessionAttr("fileName").toString());
		
		renderJson(Ret.ok("msg", "生成成功").set("imgurl", getGenDir()+getSessionAttr("fileName").toString()+".jpg"));
		
	}

	/**
	 * 
	 * @Title: downImg   
	 * @Description: TODO : 图片下载
	 * @param:       
	 * @author: 冉志林    
	 * @date:   Oct 19, 2018 6:19:37 PM  
	 * @return: void      
	 * @throws
	 */
	@ActionKey("/download")
	public void downImg(){
		HttpSession session = getRequest().getSession(false);
		if(session!=null){
			File file = new File(PathKit.getWebRootPath()+getGenDir()+getSessionAttr("fileName").toString()+".jpg");
	        if (file.exists()) { //如果文件存在
	            renderFile(file);
	        } else {
	            renderJson();
	        }
		}else{
			renderText("请先选择logo标志,添加地址信息,地址信息以：http:// 开头,然后点击生成,再点击下载即可");
		}
		
		
	}
	
	
	public void uploadImg(){
		UploadFile uf = null;
		try {
			uf = getFile("avatar", getAvatarTempDir(), getAvatarMaxSize());
			if (uf == null) {
				renderJson(Ret.fail("msg", "请先选择上传文件"));
				return;
			}
			String fileName = uf.getFileName();
			setSessionAttr("fileName", fileName);
		} catch (Exception e) {
			if (e instanceof com.jfinal.upload.ExceededSizeException) {
				renderJson(Ret.fail("msg", "文件大小超出范围"));
			} else {
				if (uf != null) {
					// 只有出现异常时才能删除，不能在 finally 中删，因为后面需要用到上传文件
					uf.getFile().delete();
				}
				renderJson(Ret.fail("msg", e.getMessage()));
			}
			return ;
		}
		Ret ret = uploadAvatar(uf);
		if (ret.isOk()) {   // 上传成功则将文件 url 径暂存起来，供下个环节进行裁切
			setSessionAttr("avatarUrl", ret.get("avatarUrl"));
		}
		renderJson(ret);
	}
	
	
	public Ret uploadAvatar(UploadFile uf) {
		if (uf == null) {
			return Ret.fail("msg", "上传文件UploadFile对象不能为null");
		}

		try {
			if (ImageKit.notImageExtName(uf.getFileName())) {
				return Ret.fail("msg", "文件类型不正确，只支持图片类型：gif、jpg、jpeg、png、bmp");
			}

			String avatarUrl = "/upload" + getAvatarTempDir()+System.currentTimeMillis() + extName;
			String saveFile = PathKit.getWebRootPath() + avatarUrl;
			ImageKit.zoom(500, uf.getFile(), saveFile);
			return Ret.ok("avatarUrl", avatarUrl);
		}
		catch (Exception e) {
			return Ret.fail("msg", e.getMessage());
		} finally {
			uf.getFile().delete();
		}
	}

}
